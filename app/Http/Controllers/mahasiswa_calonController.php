<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mahasiswa_calon;
use App\Http\Requests;
use App\majors;
use App\academic_years;
use App\kota_kabupaten;
use App\Http\Requests\mahasiswa_calon\UpdateRequest;
use App\Http\Requests\mahasiswa_calon\StoreRequest;
use Datatables;

class mahasiswa_calonController extends Controller
{
    public function index()
    {   
        
    	$mahasiswa_calon=mahasiswa_calon::all();
        return view('mahasiswa_calon.mahasiswa_calonView',compact('mahasiswa_calon'));
    }

    public function getmahasiswa_calon()
    {
        $mahasiswa_calon = mahasiswa_calon::select(['program_studi_id','tahun_ajaran_id','nama','telp','email','tempat_lahir','gender','alamat_asal','jalur_pendaftaran','reguler','hasil_seleksi'])->get();
  
        return Datatables::of($mahasiswa_calon)->make(true);
    }
    public function create()
    {
        $majors=majors::all();
        $kota_kabupaten=kota_kabupaten::all();
        $academics=academic_years::all();
        return view('mahasiswa_calon.mahasiswa_calonCreate',compact('kota_kabupaten','academics','majors'));
        //
    }
    public function store(StoreRequest $request)
    {
        $mahasiswa_calon = new mahasiswa_calon();
        $mahasiswa_calon->program_studi_id = $request->program_studi_id;
        $mahasiswa_calon->tahun_ajaran_id=$request->tahun_ajaran_id;
        $mahasiswa_calon->nama=$request->nama;
        $mahasiswa_calon->telp=$request->telp;
        $mahasiswa_calon->email=$request->email;
        $mahasiswa_calon->tempat_lahir=$request->tempat_lahir;
        $mahasiswa_calon->gender=$request->gender;
        $mahasiswa_calon->alamat_asal=$request->alamat_asal;
        $mahasiswa_calon->jalur_pendaftaran=$request->jalur_pendaftaran;
        $mahasiswa_calon->reguler=$request->reguler;
        $mahasiswa_calon->hasil_seleksi=$request->hasil_seleksi;
        $mahasiswa_calon->save();
        return redirect()->route('mahasiswa_calons.index')->with('alert-success','Data berhasil Disimpan.');
    }
    public function destroy($id)
    {
        $mahasiswa_calon = mahasiswa_calon::findOrFail($id);
        $mahasiswa_calon->delete();
        return redirect()->route('mahasiswa_calons.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
    public function edit($id)
    {
        $majors=majors::all();
        $academics=academic_years::all();
        $kota_kabupaten=kota_kabupaten::all();
        $mahasiswa_calon = mahasiswa_calon::findOrFail($id);
        return view('mahasiswa_calon.mahasiswa_calonEdit', compact('academics','kota_kabupaten','majors','mahasiswa_calon'));
    }
    public function update(UpdateRequest $request, $id)
    {
        $mahasiswa_calon = mahasiswa_calon::findOrFail($id);
        $mahasiswa_calon->program_studi_id = $request->program_studi_id;
        $mahasiswa_calon->tahun_ajaran_id=$request->tahun_ajaran_id;
        $mahasiswa_calon->nama=$request->nama;
        $mahasiswa_calon->telp=$request->telp;
        $mahasiswa_calon->email=$request->email;
        $mahasiswa_calon->tempat_lahir=$request->tempat_lahir;
        $mahasiswa_calon->gender=$request->gender;
        $mahasiswa_calon->alamat_asal=$request->alamat_asal;
        $mahasiswa_calon->jalur_pendaftaran=$request->jalur_pendaftaran;
        $mahasiswa_calon->reguler=$request->reguler;
        $mahasiswa_calon->hasil_seleksi=$request->hasil_seleksi;
        $mahasiswa_calon->save();
        return redirect()->route('mahasiswa_calons.index')->with('alert-success', 'Data Successfully Updated.');
    }
}
