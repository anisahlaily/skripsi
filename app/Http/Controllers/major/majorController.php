<?php

namespace App\Http\Controllers\major;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\majors;
use App\department;

class majorController extends Controller
{
    public function index()
    {   
        
    	$majors=majors::all();
        return view('majors.majorsView',compact('majors'));
    }
    public function create()
    {
        $departments=department::all();
        return view('majors.majorsCreate',compact('departments'));
        //
    }
    public function store(Request $request)
    {
        $majors = new majors();
        $majors->department_id = $request->department_id;
        $majors->name=$request->name;
        $majors->telp=$request->telp;
        $majors->email=$request->email;
        $majors->address=$request->address;
        $majors->facsimile=$request->facsimile;
        $majors->homepage=$request->homepage;
        $majors->save();
        return redirect()->route('majors.index')->with('alert-success','Data berhasil Disimpan.');
    }
    public function edit($id)
    {
        $departments=department::all();
        $majors = majors::findOrFail($id);
        return view('majors.majorsEdit', compact('departments','majors'));
    }
    public function update(Request $request, $id)
    {
        $majors = majors::findOrFail($id);
        $majors->department_id = $request->department_id;
        $majors->name=$request->name;
        $majors->telp=$request->telp;
        $majors->email=$request->email;
        $majors->address=$request->address;
        $majors->facsimile=$request->facsimile;
        $majors->homepage=$request->homepage;
        $majors->save();
        return redirect()->route('majors.index')->with('alert-success', 'Data Successfully Updated.');
    }
    public function destroy($id)
    {
        $majors = majors::findOrFail($id);
        $majors->delete();
        return redirect()->route('majors.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
