<?php

namespace App\Http\Controllers\mahasiswalulusan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\mahasiswa_calon;

class mahasiswalulusanController extends Controller
{
    public function index()
    {
    	//$mahasiswa_calon = mahasiswa_calon::all();
        //return view('mahasiswalulusan.simulasimahasiswalulusan',compact('mahasiswa_calon'));
       // $mahasiswa_calon = DB::table('mahasiswa_calon')->count();

        //return view('mahasiswalulusan.simulasimahasiswalulusan', ['mahasiswa_calon' => $mahasiswa_calon]);

        $mahasiswa_calon=mahasiswa_calon::count();
        return view('mahasiswalulusan.simulasimahasiswalulusan',compact('mahasiswa_calon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
