<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Lecturers extends Model
{
    protected $table='lecturers';

    public function lecturerEducation()
    {
    	return $this->hasMany('App\Lecturer_educations');
    }
}
