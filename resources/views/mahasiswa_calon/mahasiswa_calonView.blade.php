@extends('mahasiswa_calon.adminlte')
 
@section('content')
<div class="row">
<div class="col-md-8 col-md-offset-2">
    <h3 align="center">Data Calon Mahasiswa </h3>   
    <a href="{{route('mahasiswa_calonCreate')}}" class="btn btn-primary btn-md" ><span  class="glyphicon glyphicon-list-alt"></span>  Add New</a></a>  
    <div>
        <h3> </h3>
    </div>
    <table class="table table-striped" id="table">
        <thead>
            <tr>
                <td>No</td>
                <td>Program Studi</td>
                <td>Tahun Ajaran</td>
                <td>Nama</td>
                <td>Telp</td>
                <td>Email</td>
                <td>Tempat Lahir</td>
                <td>Gender</td>
                <td>Alamat Asal</td>
                <td>Jalur Pendaftaran</td>
                <td>Reguler</td>
                <td>Hasil Seleksi</td>
                <td>Aksi</td>
                
            </tr>
        </thead>
        <tbody>
            <?php $no=1; ?>
                        @foreach($mahasiswa_calon as $mahasiswa_calon)
                            <tr class="">
                            <td>{{$no++}}</td>
                            <td>{{$mahasiswa_calon->program_studi_id}}</td>
                            <td>{{$mahasiswa_calon->tahun_ajaran_id}}</td>
                            <td>{{$mahasiswa_calon->nama}}</td>
                            <td>{{$mahasiswa_calon->telp}}</td>
                            <td>{{$mahasiswa_calon->email}}</td> 
                            <td>{{$mahasiswa_calon->tempat_lahir}}</td>
                            <td>{{$mahasiswa_calon->gender}}</td>  
                            <td>{{$mahasiswa_calon->alamat_asal}}</td>
                            <td>{{$mahasiswa_calon->jalur_pendaftaran}}</td> 
                            <td>{{$mahasiswa_calon->reguler}}</td> 
                            <td>{{$mahasiswa_calon->hasil_seleksi}}</td>   
                            <td><form method="POST" action="{{ route('mahasiswa_calons.destroy', $mahasiswa_calon->id) }}" accept-charset="UTF-8">
                                <input name="_method" type="hidden" value="DELETE">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                                <a href="{{route('mahasiswa_calonEdit', $mahasiswa_calon->id)}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

                                <button type="submit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="delete"><span class="glyphicon glyphicon-trash" onclick="return confirm('Anda yakin akan menghapus data ?');" ></span>
                                    </button>
                                </form>
                            </td>
                            </tr>
                            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection
<!--@push('js')
@section('scripts') 
<script type="text/javascript">
  $(document).ready(function() {
    $('#table').DataTable(
        {
         "scrollX": true,
             processing: true,
                serverSide: true,
                //
                ajax: '{{ url("get-mahasiswa_calon") }}',   
        });
} );
 </script>

@endsection
@endpush
 
<!--@push('js')
<script type="text/javascript">
    $(function(){
        $("#data-mahasiswa_calon").DataTable({
            "scrollX": true,
             processing: true,
                serverSide: true,
                //
                ajax: '{{ url("get-mahasiswa_calon") }}',
                columns: [
                    { data: 'program_studi_id', name: 'program_studi_id' },
                    { data: 'tahun_ajaran_id', name: 'tahun_ajaran_id' },
                    { data: 'nama', name: 'nama' },
                    { data: 'telp', name: 'telp' },
                    { data: 'email', name: 'email' },
                    { data: 'tempat_lahir', name: 'tempat_lahir' },
                    { data: 'gender', name: 'gender' },
                    { data: 'alamat_asal', name: 'alamat_asal' },
                    { data: 'jalur_pendaftaran', name: 'jalur_pendaftaran' },
                    { data: 'reguler', name: 'reguler' },
                    { data: 'hasil_seleksi', name: 'hasil_seleksi' },

                ]

        });
    });
</script>

@endpush -->
