@extends('mahasiswa_calon.adminlte')
@section('content')
<div class="container-fluid">
	
				<div class="panel-body">
				<br style="clear:both">
					<h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>EDIT JOBLIST</b></h4>
                    <hr>
					<form action="{{route('mahasiswa_calons.update', $mahasiswa_calon->id)}}" method="post">
					<input name="_method" type="hidden" value="PATCH">
					{{csrf_field()}}
						<label class="w3-label w3-text-green"><b>Program Studi ID</b></label>
						<div class="form-group{{ $errors->has('program_studi_id') ? ' has-error' : '' }}">
							
					<select class="form-control m-bot15" name="program_studi_id">
                            @foreach($majors as $major)
                            <option 
                            value="{{$major->id}}"
                            @if($major->id === $mahasiswa_calon->program_studi_id)
                            selected
                            @endif
                            >
                            {{$major->name}}
                        </option>
                            @endForeach
                           
                        </select>

						</div>

						<label class="w3-label w3-text-green"><b>Tahun Ajaran ID</b></label>
						<div class="form-group{{ $errors->has('tahun_ajaran_id') ? ' has-error' : '' }}">
							<select class="form-control m-bot15" name="tahun_ajaran_id">
                            @foreach($academics as $major)
                            <option 
                            value="{{$major->id}}"
                            @if($major->id === $mahasiswa_calon->tahun_ajaran_id)
                            selected
                            @endif
                            >
                            {{$major->name}}
                        </option>
                            @endForeach
                           
                        </select>
							{!! $errors->first('tahun_ajaran_id', '<p class="help-block">:message</p>') !!}
						</div>

						<label class="w3-label w3-text-green"><b>Nama</b></label>
						<div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
							<input type="text" name="nama" class="form-control" placeholder="nama" value="{{$mahasiswa_calon->nama}}" required>
							{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
						</div>

						<label class="w3-label w3-text-green"><b>Telp</b></label>
						<div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
							<input type="text" name="telp" class="form-control" placeholder="Telp" value="{{$mahasiswa_calon->telp}}" required>
							{!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Email</b></label>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<input type="email" name="email" class="form-control" placeholder="Email" value="{{$mahasiswa_calon->email}}" required>
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Tempat Lahir</b></label>
						<div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
							<select class="form-control m-bot15" name="tempat_lahir">
                            @foreach($kota_kabupaten as $major)
                            <option 
                            value="{{$major->id}}"
                            @if($major->id === $mahasiswa_calon->tempat_lahir)
                            selected
                            @endif
                            >
                            {{$major->nama_kota_kabupaten}}
                        </option>
                            @endForeach
                           
                        </select>
							{!! $errors->first('tempat_lahir', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Gender</b></label>
						<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
							<input type="text" name="gender" class="form-control" placeholder="Gender" value="{{$mahasiswa_calon->gender}}" required>
							{!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Alamat Asal</b></label>
						<div class="form-group{{ $errors->has('alamat_asal') ? ' has-error' : '' }}">
							<input type="text" name="alamat_asal" class="form-control" placeholder="Alamat Asal" value="{{$mahasiswa_calon->alamat_asal}}" required>
							{!! $errors->first('alamat_asal', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Jalur Pendaftaran</b></label>
						<div class="form-group{{ $errors->has('jalur_pendaftaran') ? ' has-error' : '' }}">
							<input type="text" name="jalur_pendaftaran" class="form-control" placeholder="Jalur Pendaftaran" value="{{$mahasiswa_calon->jalur_pendaftaran}}" required>
							{!! $errors->first('jalur_pendaftaran', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Reguler</b></label>
						<div class="form-group{{ $errors->has('reguler') ? ' has-error' : '' }}">
							<input type="text" name="reguler" class="form-control" placeholder="Reguler" value="{{$mahasiswa_calon->reguler}}" required>
							{!! $errors->first('reguler', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Hasil Seleksi</b></label>
						<div class="form-group{{ $errors->has('hasil_seleksi') ? ' has-error' : '' }}">
							<input type="text" name="hasil_seleksi" class="form-control" placeholder="Hasil Seleksi" value="{{$mahasiswa_calon->hasil_seleksi}}" required>
							{!! $errors->first('hasil_seleksi', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<button type="submit" class="btn button btn-primary pull-right"><span class="glyphicon glyphicon-pencil">Edit</span></button>

							<a href="{{route('mahasiswa_calons.index')}}" class="btn button  btn-primary pull-left"><span class="glyphicon glyphicon-arrow-left"></span></a>
						</div>
					</form>
				
		
	</div>
</div>
@endsection