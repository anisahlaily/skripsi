@extends('mahasiswa_calon.adminlte')

@section('content')
<div class="container-fluid">
    
                <div class="panel-body">
                <br style="clear:both">
                    <h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>Tambah Mahasiwa</b></h4>
                    <hr>
            <form action="{{route('mahasiswa_calons.store')}}" method="post">
                    {{csrf_field()}}
                    <label class="w3-label w3-text-green"><b>Program Studi ID</b></label>
                        <div class="form-group{{ $errors->has('program_studi_id') ? ' has-error' : '' }}">
                        <select class="form-control m-bot15" name="program_studi_id">
                            @if($majors->count() > 0)
                            @foreach($majors as $major)
                            <option value="{{$major->id}}">{{$major->name}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                    </div>
                    <label class="w3-label w3-text-green"><b>Tahun Ajaran ID</b></label>
                        <div class="form-group{{ $errors->has('tahun_ajaran_id') ? ' has-error' : '' }}">
                           <!-- <input type="text" name="tahun_ajaran_id" class="form-control" placeholder="tahun_ajaran_id" required>
                            {!! $errors->first('tahun_ajaran_id', '<p class="help-block">:message</p>') !!}-->
                            <select class="form-control m-bot15" name="tahun_ajaran_id">
                            @if($academics->count() > 0)
                            @foreach($academics as $major)
                            <option value="{{$major->id}}">{{$major->name}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                        </div>
                        <label class="w3-label w3-text-green"><b>nama</b></label>
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <input type="text" name="nama" class="form-control" placeholder="nama" required>
                        
                            {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Telp</b></label>
                        <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                            <input type="text" name="telp" class="form-control" placeholder="telp" required>
                        
                            {!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Email</b></label>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="email" required>
                        
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Tempat Lahir</b></label>
                        <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
                            <select class="form-control m-bot15" name="tempat_lahir">
                            @if($kota_kabupaten->count() > 0)
                            @foreach($kota_kabupaten as $kota)
                            <option value="{{$kota->id}}">{{$kota->nama_kota_kabupaten}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                        </div>
                        <label class="w3-label w3-text-green"><b>Gender</b></label>
                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <input type="text" name="gender" class="form-control" placeholder="gender" required>
                        
                            {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Alamat Asal</b></label>
                        <div class="form-group{{ $errors->has('alamat_asal') ? ' has-error' : '' }}">
                            <input type="text" name="alamat_asal" class="form-control" placeholder="alamat_asal" required>
                        
                            {!! $errors->first('alamat_asal', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Jalur Pendaftaran</b></label>
                        <div class="form-group{{ $errors->has('jalur_pendaftaran') ? ' has-error' : '' }}">
                            <input type="text" name="jalur_pendaftaran" class="form-control" placeholder="jalur_pendaftaran" required>
                        
                            {!! $errors->first('jalur_pendaftaran', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Reguler</b></label>
                        <div class="form-group{{ $errors->has('reguler') ? ' has-error' : '' }}">
                            <input type="text" name="reguler" class="form-control" placeholder="reguler" required>
                        
                            {!! $errors->first('reguler', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Hasil Seleksi</b></label>
                        <div class="form-group{{ $errors->has('hasil_seleksi') ? ' has-error' : '' }}">
                            <input type="text" name="hasil_seleksi" class="form-control" placeholder="hasil_seleksis" required>
                        
                            {!! $errors->first('hasil_seleksi', '<p class="help-block">:message</p>') !!}
                        </div>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                    

                    </form>
        </div>
    </div>
       
@endsection