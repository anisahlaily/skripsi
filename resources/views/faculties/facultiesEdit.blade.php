@extends('faculties.adminlte')
@section('content')
<div class="container-fluid">
	
				<div class="panel-body">
				<br style="clear:both">
					<h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>EDIT JOBLIST</b></h4>
                    <hr>
					<form action="{{route('faculties.update',$faculties->id)}}" method="post">
					<input name="_method" type="hidden" value="PATCH">
					{{csrf_field()}}
						<label class="w3-label w3-text-green"><b>Nama</b></label>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<input type="text" name="name" class="form-control" placeholder="name" value="{{$faculties->name}}" required>
							{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>University</b></label>
						<div class="form-group{{ $errors->has('university') ? ' has-error' : '' }}">
							<input type="text" name="university" class="form-control" placeholder="Jalur Pendaftaran" value="{{$faculties->university}}" required>
							{!! $errors->first('university', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Telp</b></label>
						<div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
							<input type="text" name="telp" class="form-control" placeholder="Telp" value="{{$faculties->telp}}" required>
							{!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Email</b></label>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<input type="email" name="email" class="form-control" placeholder="Email" value="{{$faculties->email}}" required>
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Founded On</b></label>
						<div class="form-group{{ $errors->has('founded_on') ? ' has-error' : '' }}">
							<input type="date" name="founded_on" class="form-control" placeholder="Alamat Asal" value="{{$faculties->founded_on}}" required>
							{!! $errors->first('founded_on', '<p class="help-block">:message</p>') !!}
						</div>
						
						<label class="w3-label w3-text-green"><b>City</b></label>
						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<select class="form-control m-bot15" name="city">
                            @foreach($kota_kabupaten as $major)
                            <option 
                            value="{{$major->id}}"
                            @if($major->id === $faculties->city)
                            selected
                            @endif
                            >
                            {{$major->nama_kota_kabupaten}}
                        </option>
                            @endForeach
                           
                        </select>
							{!! $errors->first('city', '<p class="help-block">:message</p>') !!}
						</div>
						
						<div class="form-group">
							<button type="submit" class="btn button btn-primary pull-right"><span class="glyphicon glyphicon-pencil">Edit</span></button>

							<a href="{{route('faculties.index')}}" class="btn button  btn-primary pull-left"><span class="glyphicon glyphicon-arrow-left"></span></a>
						</div>
					</form>
				
		
	</div>
</div>
@endsection