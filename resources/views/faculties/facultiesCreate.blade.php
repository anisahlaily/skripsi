@extends('faculties.adminlte')

@section('content')
<div class="container-fluid">
    
                <div class="panel-body">
                <br style="clear:both">
                    <h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>Tambah Fakultas</b></h4>
                    <hr>
            <form action="{{route('faculties.store')}}" method="post">
                    {{csrf_field()}}
                        <label class="w3-label w3-text-green"><b>name</b></label>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" name="name" class="form-control" placeholder="name" required>
                        
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>University</b></label>
                        <div class="form-group{{ $errors->has('university') ? ' has-error' : '' }}">
                            <input type="text" name="university" class="form-control" placeholder="university" required>
                        
                            {!! $errors->first('university', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Telp</b></label>
                            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                            <input type="telp" name="telp" class="form-control" placeholder="telp" required>
                        
                            {!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <label class="w3-label w3-text-green"><b>Email</b></label>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="email" required>
                        
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Founded On</b></label>
                        <div class="form-group{{ $errors->has('founded_on') ? ' has-error' : '' }}">
                            <input type="date" name="founded_on" class="form-control" placeholder="founded_on" required>
                        
                            {!! $errors->first('founded_on', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>City</b></label>
                        <div class="form-group{{ $errors->has('homepage') ? ' has-error' : '' }}">
                            <select class="form-control m-bot15" name="city">
                            @if($kota_kabupaten->count() > 0)
                            @foreach($kota_kabupaten as $kota)
                            <option value="{{$kota->id}}">{{$kota->nama_kota_kabupaten}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                        </div>
                        
                            <input type="submit" class="btn btn-primary" value="Simpan">
                       

                    </form>
         </div>
     </div>

        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection