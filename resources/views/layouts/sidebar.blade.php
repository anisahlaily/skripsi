<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard Sistem</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Simulasi</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Akreditasi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Print Report</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Report Nilai Simulasi</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Report Nilai Akreditasi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="">
            <i class="fa fa-link"></i>
            <span>Nilai Akreditasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href=""><i class="fa fa-graduation-cap"></i> Mahasiswa dan Lulusan</a></li>   
              <li><a href="{{('lecturers')}}"><i class="fa fa-suitcase"></i> Dosen dan T.Akademik</a></li>
              <li><a href=""><i class="fa fa-book"></i> Kurikulum dan Akademik</a></li>
              <li><a href=""><i class="fa fa-home"></i> Sarana Dan Pra-Sarana</a></li>
              <li><a href="{{('lecturerResearch')}}"><i class="fa fa-book"></i> Penelitian Dan Pengabdian</a></li>         
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Simulasi Akreditasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{('mahasiswalulusanIndex')}}"><i class="fa fa-circle-o"></i> Mahasiswa dan Lulusan</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Dosen Dan Tenaga Akademik</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Kurikulum Dan Akademik</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Sarana Dan Pra-Sarana</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Dosen Dan Tenaga Akademik</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Penelitian Dan Pengabdian</a></li>
          </ul>
        </li>
        <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>