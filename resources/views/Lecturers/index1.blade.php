@extends('adminlte')
@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="box">
		<div class="box-header with-border">
			<div class="box-title"><h1><span class="label label-primary">Standar 4 Sumber Daya Manusia</span></h1></div>			
		</div>
		<div class="box-body">
    <table class="table table-bordered">
      <tr>
        <th><h3><span class="label label-primary">Kualifikasi Dosen</span></h3></th>
        <th><h3><span class="label label-primary">Kualifikasi Dosen Tidak Tetap</span></h3></th>
        <th><h3><span class="label label-primary">Upaya Peningkatan Kualitas SDM</span></h3></th>
        <th><h3><span class="label label-primary">Kualifikasi Tenaga Pendidikan</span></h3></th>
      </tr>
      <tr>
        <td>
					<h5>	<span class="label label-info">Dosen Tetap Pendidikan S2 dan S3</span></h5>
					<h5>	<span class="label label-info">Dosen Tetap Pendidikan S3 sesuai PS</span></h5>
					<h5>	<span class="label label-info">Dosen Tetap Jabaran Lektor dan Guru Besar</span></h5>
					<h5>	<span class="label label-info">Dosen Sertifikat Pendidik Profesional</span></h5>
					<h5>	<span class="label label-info">Rasio Mahasiswa dan Dosen Tetap</span></h5>
					<h5>	<span class="label label-info">Beban Ajar Dosen per Semester</span></h5>
					<h5>	<span class="label label-info">Pendidikan Terahir Dosen dan Matakuliah Ajar</span></h5>
					<h5>	<span class="label label-info">Jumlah Kehadiran Dosen</span></h5>
        </td>
        <td>
        	<h5> <span class="labe label-warning">Persentase Jumlah Dosen Tidak Tetap</span></h5>
        	<h5> <span class="label label-warning">Kesesuaian Dosen Tidak tetap dg Matakuliah Ajar</span></h5>
        	<h5> <span class="label label-warning">Tingkat Kehadiran Dosen Dalam Mengajar</span></h5>
        </td>
        <td>
        	<h5> <span class="label label-success">Kegiatan Tenaga Ahli / Pakar</span></h5>
        	<h5> <span class="label label-success">Tugas Belajar Dosen Sesuai PS</span></h5>
        	<h5> <span class="label label-success">Kegiatan Dosen Tetap Dalam Seminar Sesuai PS</span></h5>
        </td>
        <td>
        	<h5> <span class="label label-danger">Pustakawan dan Kualifikasi nya</span></h5>
        	<h5> <span class="label label-danger">Tenaga Administrasi</span></h5>
        </td>
      </tr>
    </table>
		</div>
	</div>
	</div>
</div>
@endsection