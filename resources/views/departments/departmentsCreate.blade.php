@extends('departments.adminlte')

@section('content')
<div class="container-fluid">
    
                <div class="panel-body">
                <br style="clear:both">
                    <h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>Tambah Department</b></h4>
                    <hr>
            <form action="{{route('departments.store')}}" method="post">
                    {{csrf_field()}}
                        <label class="w3-label w3-text-green"><b>Faculty ID</b></label>
                        <div class="form-group{{ $errors->has('faculty_id') ? ' has-error' : '' }}">
                            <select class="form-control m-bot15" name="faculty_id">
                            @if($faculties->count() > 0)
                            @foreach($faculties as $data)
                            <option value="{{$data->id}}">{{$data->name}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                        
                            {!! $errors->first('faculty_id', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Name</b></label>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" name="name" class="form-control" placeholder="name" required>
                        
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Telp</b></label>
                            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                            <input type="telp" name="telp" class="form-control" placeholder="telp" required>
                        
                            {!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <label class="w3-label w3-text-green"><b>Email</b></label>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="email" required>
                        
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Address</b></label>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <input type="text" name="address" class="form-control" placeholder="address" required>
                        
                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Facsimile</b></label>
                        <div class="form-group{{ $errors->has('facsimile') ? ' has-error' : '' }}">
                            <input type="text" name="facsimile" class="form-control" placeholder="facsimile" required>
                        
                            {!! $errors->first('facsimile', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Homepage</b></label>
                        <div class="form-group{{ $errors->has('homepage') ? ' has-error' : '' }}">
                            <input type="text" name="homepage" class="form-control" placeholder="homepage" required>
                        
                            {!! $errors->first('homepage', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Faculty Name</b></label>
                        <div class="form-group{{ $errors->has('faculty_name') ? ' has-error' : '' }}">
                            <input type="text" name="faculty_name" class="form-control" placeholder="faculty_name" required>
                        
                            {!! $errors->first('faculty_name', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>University</b></label>
                        <div class="form-group{{ $errors->has('university') ? ' has-error' : '' }}">
                            <input type="text" name="university" class="form-control" placeholder="university" required>
                        
                            {!! $errors->first('university', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>City</b></label>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <select class="form-control m-bot15" name="city">
                            @if($kota_kabupaten->count() > 0)
                            @foreach($kota_kabupaten as $data)
                            <option value="{{$data->id}}">{{$data->nama_kota_kabupaten}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                        
                            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>User ID</b></label>
                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <select class="form-control m-bot15" name="user_id">
                            @if($users->count() > 0)
                            @foreach($users as $data)
                            <option value="{{$data->id}}">{{$data->name}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                        
                            {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
                        </div>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                       

                    </form>
         </div>
     </div>

        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection