@extends('departments.adminlte')
@section('content')
<div class="container-fluid">
	
				<div class="panel-body">
				<br style="clear:both">
					<h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>EDIT JOBLIST</b></h4>
                    <hr>
					<form action="{{route('departments.update',$departments->id)}}" method="post">
					<input name="_method" type="hidden" value="PATCH">
					{{csrf_field()}}
						<label class="w3-label w3-text-green"><b>Facilty ID</b></label>
						<div class="form-group{{ $errors->has('faculty_id') ? ' has-error' : '' }}">
							<select class="form-control m-bot15" name="faculty_id">
                            @foreach($faculties as $data)
                            <option 
                            value="{{$data->id}}"
                            @if($data->id === $departments->faculty_id)
                            selected
                            @endif
                            >
                            {{$data->name}}
                        </option>
                            @endForeach
                           
                        </select>
							{!! $errors->first('faculty_id', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Name</b></label>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<input type="text" name="name" class="form-control" placeholder="name" value="{{$departments->name}}" required>
							{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Telp</b></label>
						<div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
							<input type="text" name="telp" class="form-control" placeholder="Telp" value="{{$departments->telp}}" required>
							{!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Email</b></label>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<input type="email" name="email" class="form-control" placeholder="Email" value="{{$departments->email}}" required>
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Address</b></label>
						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<input type="date" name="address" class="form-control" placeholder="Address" value="{{$departments->address}}" required>
							{!! $errors->first('address', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Facimile</b></label>
						<div class="form-group{{ $errors->has('facimile') ? ' has-error' : '' }}">
							<input type="date" name="facimile" class="form-control" placeholder="Facimile" value="{{$departments->facimile}}" required>
							{!! $errors->first('address', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Homepage</b></label>
						<div class="form-group{{ $errors->has('homepage') ? ' has-error' : '' }}">
							<input type="date" name="homepage" class="form-control" placeholder="Homepage" value="{{$departments->homepage}}" required>
							{!! $errors->first('homepage', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Homepage</b></label>
						<div class="form-group{{ $errors->has('homepage') ? ' has-error' : '' }}">
							<input type="date" name="homepage" class="form-control" placeholder="Homepage" value="{{$departments->homepage}}" required>
							{!! $errors->first('homepage', '<p class="help-block">:message</p>') !!}
						</div>
						
						
						<div class="form-group">
							<button type="submit" class="btn button btn-primary pull-right"><span class="glyphicon glyphicon-pencil">Edit</span></button>

							<a href="{{route('faculties.index')}}" class="btn button  btn-primary pull-left"><span class="glyphicon glyphicon-arrow-left"></span></a>
						</div>
					</form>
				
		
	</div>
</div>
@endsection