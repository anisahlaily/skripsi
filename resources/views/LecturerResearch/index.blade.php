@extends('adminlte')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<div class="box-title"><h1><span class="label label-primary">Standar 7 Penelitian dan Pengabdian</span></h1></div>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
			      <tr>
			        <th><h3><span class="label label-primary">Produktifitas Dan Mutu Penelitian</span></h3></th>
			        <th><h3><span class="label label-primary">Kegiatan Penelitian Dan Pengabdian</span></h3></th>
			        <th><h3><span class="label label-primary">Jumlah Dan Mutu Kerja Sama</span></h3></th>
			      </tr>
			      <tr>
			      	<td>
			      		<h5><span class="label label-info">Jumlah Penelitian Sesuai PS dalam 3 Tahun</span></h5>
			      		<h5><span class="label label-info">Keterlibatan Mahasiswa-Dosen Dalam Penelitian</span></h5>
			      		<h5><span class="label label-info">Jumlah Artikel Ilmiah Dosen Dalam 3 Tahun</span></h5>
			      	</td>
			      	<td>
			      		<h5><span class="label label-info">Jumlah Penelitian yg di Lakukan Dosen Tetap</span></h5>
			      	</td>
			      	<td>
			      		<h5><span class="label label-warning">Mutu Kejra Sama</span></h5>
			      	</td>
			      </tr>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection