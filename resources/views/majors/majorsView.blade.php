@extends('majors.adminlte')
 
@section('content')
<div class="row">
<div class="col-md-8 col-md-offset-2">
    <h3 align="center">Data Calon Mahasiswa </h3>   
    <a href="{{route('majorsCreate')}}" class="btn btn-primary btn-md" ><span  class="glyphicon glyphicon-list-alt"></span>  Add New</a></a>  
    <div>
        <h3> </h3>
    </div>
    <table class="table table-striped" id="table">
        <thead>
            <tr>
                <td>No</td>
                <td>Departemen ID</td>
                <td>Name</td>
                <td>Telp</td>
                <td>Email</td>
                <td>Address</td>
                <td>Facsimile</td>
                <td>Homepage</td>
                <td>Aksi</td>
                
            </tr>
        </thead>
        <tbody>
            <?php $no=1; ?>
                        @foreach($majors as $data)
                            <tr class="">
                            <td>{{$no++}}</td>
                            <td>{{$data->department_id}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->telp}}</td>
                            <td>{{$data->email}}</td> 
                            <td>{{$data->address}}</td>
                            <td>{{$data->facsimile}}</td>  
                            <td>{{$data->homepage}}</td>   
                            <td><form method="POST" action="{{ route('majors.destroy', $data->id) }}" accept-charset="UTF-8">
                                <input name="_method" type="hidden" value="DELETE">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                                <a href="{{route('majors.edit', $data->id)}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>


                                <button type="submit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="delete"><span class="glyphicon glyphicon-trash" onclick="return confirm('Anda yakin akan menghapus data ?');" ></span>
                                    </button>
                                </form>
                            </td>
                            </tr>
                            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection
