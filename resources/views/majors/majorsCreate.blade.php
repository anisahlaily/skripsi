@extends('majors.adminlte')

@section('content')
<div class="container-fluid">
    
                <div class="panel-body">
                <br style="clear:both">
                    <h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>EDIT JOBLIST</b></h4>
                    <hr>
            <form action="{{route('majors.store')}}" method="post">
                    {{csrf_field()}}
                    <label class="w3-label w3-text-green"><b>Department</b></label>
                        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
                        <select class="form-control m-bot15" name="department_id">
                            @if($departments->count() > 0)
                            @foreach($departments as $department)
                            <option value="{{$department->id}}">{{$department->name}}</option>
                            @endForeach
                            @else
                            No Record Found
                            @endif   
                        </select>
                    </div>
                        <label class="w3-label w3-text-green"><b>name</b></label>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" name="name" class="form-control" placeholder="name" required>
                        
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Telp</b></label>
                        <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                            <input type="text" name="telp" class="form-control" placeholder="telp" required>
                        
                            {!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Email</b></label>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="email" required>
                        
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <label class="w3-label w3-text-green"><b>Address</b></label>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <input type="text" name="address" class="form-control" placeholder="address" required>
                        
                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Facsimile</b></label>
                        <div class="form-group{{ $errors->has('facsimile') ? ' has-error' : '' }}">
                            <input type="text" name="facsimile" class="form-control" placeholder="facsimile" required>
                        
                            {!! $errors->first('facsimile', '<p class="help-block">:message</p>') !!}
                        </div>
                        <label class="w3-label w3-text-green"><b>Homepage</b></label>
                        <div class="form-group{{ $errors->has('homepage') ? ' has-error' : '' }}">
                            <input type="text" name="homepage" class="form-control" placeholder="homepage" required>
                        
                            {!! $errors->first('homepage', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                            <input type="submit" class="btn btn-primary" value="Simpan">
                       

                    </form>
         </div>
     </div>

        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection