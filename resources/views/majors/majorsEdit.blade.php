@extends('majors.adminlte')
@section('content')
<div class="container-fluid">
	
				<div class="panel-body">
				<br style="clear:both">
					<h4 class="w3-label w3-text-green" style="margin-bottom: 25px; text-align: center;"><b>EDIT JOBLIST</b></h4>
                    <hr>
					<form action="{{route('majors.update',$majors->id)}}" method="post">
					<input name="_method" type="hidden" value="PATCH">
					{{csrf_field()}}
						<label class="w3-label w3-text-green"><b>Departement ID</b></label>
						<div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
							
					<select class="form-control m-bot15" name="department_id">
                            @foreach($departments as $department)
                            <option 
                            value="{{$department->id}}"
                            @if($department->id === $majors->department_id)
                            selected
                            @endif
                            >
                            {{$department->name}}
                        </option>
                            @endForeach
                           
                        </select>

						</div>
						<label class="w3-label w3-text-green"><b>Nama</b></label>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<input type="text" name="name" class="form-control" placeholder="name" value="{{$majors->name}}" required>
							{!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
						</div>

						<label class="w3-label w3-text-green"><b>Telp</b></label>
						<div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
							<input type="text" name="telp" class="form-control" placeholder="Telp" value="{{$majors->telp}}" required>
							{!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Email</b></label>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<input type="email" name="email" class="form-control" placeholder="Email" value="{{$majors->email}}" required>
							{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Address</b></label>
						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<input type="text" name="address" class="form-control" placeholder="address" value="{{$majors->address}}" required>
							{!! $errors->first('address', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Facsimile</b></label>
						<div class="form-group{{ $errors->has('facsimile') ? ' has-error' : '' }}">
							<input type="text" name="facsimile" class="form-control" placeholder="Alamat Asal" value="{{$majors->facsimile}}" required>
							{!! $errors->first('facsimile', '<p class="help-block">:message</p>') !!}
						</div>
						<label class="w3-label w3-text-green"><b>Homepage</b></label>
						<div class="form-group{{ $errors->has('homepage') ? ' has-error' : '' }}">
							<input type="text" name="homepage" class="form-control" placeholder="Jalur Pendaftaran" value="{{$majors->homepage}}" required>
							{!! $errors->first('homepage', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<button type="submit" class="btn button btn-primary pull-right"><span class="glyphicon glyphicon-pencil">Edit</span></button>

							<a href="{{route('majors.index')}}" class="btn button  btn-primary pull-left"><span class="glyphicon glyphicon-arrow-left"></span></a>
						</div>
					</form>
				
		
	</div>
</div>
@endsection