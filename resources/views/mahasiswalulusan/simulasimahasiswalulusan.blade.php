@extends('mahasiswalulusan.adminlte')

@section('content')
<link rel="stylesheet" href="{{asset('css/table.css')}}">
<div class="table-wrapper">
    <table class="demo-table">
        <caption class="title">Standar Mahasiswa Dan Lulusan</caption>
        <thead>
            <tr>
                <th>KPI 3.1</th>
                <th>KPI 3.2</th>
                <th>KPI 3.3</th>
                <th>KPI 3.4</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>KPI 3.1.1</b></td>
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td><a href="#">KPI 3.1.1a </a><br><br>Nilai :  {{$mahasiswa_calon}} &emsp;&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td><a href="#">KPI 3.1.1b</a><br><br>Nilai :   &emsp;&nbsp; </td>
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
            <tr>
                <td><a href="#">KPI 3.1.1c</a><br><br>Nilai :   &emsp;&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
           
           
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Total</th>
                <th></th>
            </tr>
        </tfoot>
    </table>

    </div>

   
    </div>
@endsection