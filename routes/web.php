<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('adminlte');
});

Route::group(['middleware'=>'web'],function(){
	Route::auth();
});
Route::group(['middleware'=>['web','auth']], function(){
	Route::get('/home', 'HomeController@index');
	Route::get('/',function(){
		if(Auth::user()){
			return view('mahasiswa_calon.adminlte');
		}else{
			return view('home1');
		}
	});
});


	Route::resource('mahasiswa_calons', 'mahasiswa_calonController');
    Route::get('mahasiswa_calon/',['as' => 'mahasiswa_calon', 'uses' => 'mahasiswa_calonController@index']);
    Route::get('mahasiswa_calonCreate/',['as' => 'mahasiswa_calonCreate', 'uses' => 'mahasiswa_calonController@create']);
    Route::get('mahasiswa_calonEdit/{id}',['as' => 'mahasiswa_calonEdit', 'uses' => 'mahasiswa_calonController@edit']);

    Route::resource('faculties', 'faculties\facultiesController');
    Route::get('facultiesIndex/',['as' => 'facultiesIndex', 'uses' => 'faculties\facultiesController@index']);
    Route::get('facultiesCreate/',['as' => 'facultiesCreate', 'uses' => 'faculties\facultiesController@create']);
    Route::get('facultiesEdit/{id}',['as' => 'facultiesEdit', 'uses' => 'facultiesController@edit']);

    Route::resource('majors', 'major\majorController');
    Route::get('majorsIndex/',['as' => 'majorsIndex', 'uses' => 'major\majorController@index']);
    Route::get('majorsCreate/',['as' => 'majorsCreate', 'uses' => 'major\majorController@create']);
    Route::get('majorsEdit/',['as' => 'majorsEdit', 'uses' => 'major\majorController@edit']);

    Route::resource('departments', 'departments\departmentsController');
    Route::get('departmentsIndex/',['as' => 'departmentsIndex', 'uses' => 'departments\departmentsController@index']);
    Route::get('departmentsCreate/',['as' => 'departmentsCreate', 'uses' => 'departments\departmentsController@create']);
    Route::get('departmentsEdit/{id}',['as' => 'departmentsEdit', 'uses' => 'departments\departmentsController@edit']);

Route::resource('portal','portalController');


Route::resource('mahasiswalulusan', 'mahasiswalulusan\mahasiswalulusanController');
 Route::get('mahasiswalulusanIndex/',['as' => 'mahasiswalulusanIndex', 'uses' => 'mahasiswalulusan\mahasiswalulusanController@index']);


/*  Lecturer Standar 4   */
Route::resource('lecturers','Akreditasi\Lecturer\StandartController');

/* Penelitian Standar 7 */
Route::resource('lecturerResearch', 'Akreditasi\LecturerResearch\StandartResearchController');