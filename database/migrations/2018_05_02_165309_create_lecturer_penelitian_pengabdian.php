<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerPenelitianPengabdian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_penelitian_pengabdian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('academic_year_id')->unsigned()->index();
            $table->enum('jenis',array('PNLT','PNBD'));
            $table->string('judul');
            $table->enum('jenis_sumber_dana',array('INT','EKST'));
            $table->enum('detail_sumber_dana',array('SDR','PT_SDR','DEPDIKNAS','INST_DT','INST_LN'));
            $table->integer('jumlah_dana');
            $table->enum('keterlibatan_mahasiswa',array('Y','N'));
            $table->integer('jumlah_mahasiswa');
            $table->text('pastisipasi_mahasiswa');
            $table->string('bukti');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_penelitian_pengabdian');
    }
}
