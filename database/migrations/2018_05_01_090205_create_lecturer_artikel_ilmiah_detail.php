<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerArtikelIlmiahDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_artikel_ilmiah_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artikel_id')->unsigned()->index();
            $table->integer('lecturer_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('artikel_id')->references('id')->on('lecturer_artikel_ilmiah');
            $table->foreign('lecturer_id')->references('id')->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_artikel_ilmiah_detail');
    }
}
