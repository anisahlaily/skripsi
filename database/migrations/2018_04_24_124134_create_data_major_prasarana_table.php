<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorPrasaranaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_prasarana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->enum('kategori',array('KANTOR_PS','RUANG_KELAS','LAB','PERPUSTAKAAN','PENUNJANG'));
            $table->string('nama');
            $table->string('fungsi');
            $table->float('luas');
            $table->integer('kapasitas');
            $table->enum('kepemilikan',array('SD','SW'));
            $table->enum('perawatan',array('Y','N'));
            $table->integer('utilisasi');
            $table->enum('akses_internet',array('Y','N'));
            $table->string('lokasi');
            $table->string('pengelola');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_prasarana');
    }
}
