<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerPenelitianPengabdianDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_penelitian_pengabdian_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pnlt_pnbd_id')->unsigned()->index();
            $table->integer('lecturer_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pnlt_pnbd_id')->references('id')->on('lecturer_penelitian_pengabdian');
            $table->foreign('lecturer_id')->references('id')->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_penelitian_pengabdian_detail');
    }
}
