<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorTracerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_tracer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->enum('upaya_pelacakan',array('Y','N'));
            $table->text('deskripsi');
            $table->text('partisipasi_akademik');
            $table->text('partisipasi_nonakademik');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_tracer');
    }
}
