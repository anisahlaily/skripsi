<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerHaki extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_haki', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('jenis',array('PATEN','MEREK','CIPTA','INDUSTRI','GEOGRAFI','DTST','RD'));
            $table->integer('academic_year_id')->unsigned()->index();
            $table->string('nomor');
            $table->string('judul');
            $table->text('deskripsi');
            $table->date('tanggal');
            $table->string('bukti');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_haki');
    }
}
