<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorStudentAchievementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_student_achievement_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('achievement_id')->unsigned()->index();
            $table->integer('student_id')->unsigned()->index();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('achievement_id')->references('id')->on('data_major_student_achievements');
            $table->foreign('student_id')->references('id')->on('data_major_students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_student_achievement_details');
    }
}
