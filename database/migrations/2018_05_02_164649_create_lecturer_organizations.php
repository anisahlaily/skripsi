<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerOrganizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lecturer_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->enum('level',array('LOKAL','NASIONAL','INTERNASIONAL','PT'));
            $table->string('name');
            $table->string('last_post');
            $table->date('date_start');
            $table->date('date_end');
            $table->string('proof');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('lecturer_id')->references('id')->on('lecturers');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_organizations');
    }
}
