<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorCooperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_cooperations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->enum('kategori',array('DN','LN'));
            $table->string('nama_instansi');
            $table->string('kegiatan');
            $table->integer('tahun_awal');
            $table->integer('tahun_akhir');
            $table->text('manfaat');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_cooperations');
    }
}
