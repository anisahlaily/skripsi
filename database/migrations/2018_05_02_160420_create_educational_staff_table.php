<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationalStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educational_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->string('name');
            $table->enum('type',array('PUSTAKARAN','PARTIMER','ADMINISTRASI','LAINNYA','LABORAN','ANALIS','TEKNISI','PROGRAMMER'));
            $table->enum('level',array('PS','JURUSAN','FAKULTAS','PT'));
            $table->enum('education',array('SMAK','D1','D2','D3','D4','S1','S2','S3'));
            $table->string('unit');
            $table->enum('gender',array('M','F'));
            $table->string('proof');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educational_staff');
    }
}
