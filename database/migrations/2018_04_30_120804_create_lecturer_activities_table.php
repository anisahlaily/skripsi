<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lecturer_id')->unsigned()->index()->notnull();
            $table->integer('academic_year_id')->unsigned()->index()->notnull();
            $table->string('type')->notnull();
            $table->string('name')->notnull();
            $table->date('date');
            $table->string('penyaji')->notnull();
            $table->string('proof');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('lecturer_id')->references('id')->on('lecturers');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_activities');
    }
}
