<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorStudentAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_student_achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->enum('type',array('ILMIAH','OLAHRAGA','SENI'));
            $table->enum('level',array('LPT','WLY','NSN','INT'));
            $table->string('name');
            $table->string('achievement');
            $table->date('date');
            $table->string('proof');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_id')->references('id')->on('data_major_students');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_student_achievements');
    }
}
