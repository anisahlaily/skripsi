<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->integer('period_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->string('status');
            $table->string('modification');
            $table->text('reason');
            $table->string('proposer');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('period_id')->references('id')->on('scoring_periods');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_reviews');
    }
}
