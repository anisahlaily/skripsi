<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerArtikelIlmiah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_artikel_ilmiah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('keilmuan_id');
            $table->integer('academic_year_id')->unsigned()->index()->notnull();
            $table->string('judul')->notnull();
            $table->string('dipublikasikan_pada')->notnull();
            $table->string('tahun_publikasi')->notnull();
            $table->string('tingkat')->notnull();
            $table->string('bukti');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_artikel_ilmiah');
    }
}
