<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorSaranaLabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_sarana_lab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('period_id')->unsigned()->index();
            $table->integer('lab_id')->unsigned()->index();
            $table->string('nama');
            $table->integer('jumlah');
            $table->enum('kepemilikan',array('SD','SW'));
            $table->enum('perawatan',array('Y','N'));
            $table->integer('rata_penggunaan');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('period_id')->references('id')->on('scoring_periods');
            $table->foreign('lab_id')->references('id')->on('lab');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_sarana_lab');
    }
}
