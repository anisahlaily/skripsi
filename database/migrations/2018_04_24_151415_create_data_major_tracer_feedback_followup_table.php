<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorTracerFeedbackFollowupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_tracer_feedback_followup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->integer('period_id')->unsigned()->index();
            $table->text('integritas');
            $table->text('keahlian');
            $table->text('bahasa_inggris');
            $table->text('penggunaan_it');
            $table->text('komunikasi');
            $table->text('kerjasama_tim');
            $table->text('pengembangan_diri');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_tracer_feedback_followup');
    }
}
