<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->string('nama');
            $table->integer('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->enum('agama',array('ISLAM','KRISTEN','KATOLIK','BUDHA','HINDU','KONGHUCU'));
            $table->string('telp');
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->text('alamat_sekarang');
            $table->enum('gender',array('F','M'));
            $table->enum('status_nikah',array('MARRIED','SINGLE'));
            $table->enum('status_dosen',array('ACTIVE','RETIRED'));
            $table->enum('kepangkatan',array('Y','N'));
            $table->enum('sertifikasi',array('Y','N'));
            $table->enum('dosen_tetap',array('Y','N'));
            $table->string('nidn');
            $table->string('nidk');
            $table->string('nupn');
            $table->string('nip');
            $table->string('schopus');
            $table->string('scholar');
            $table->string('sinta');
            $table->integer('jabatan_akademik');
            $table->enum('bidang_sesuai_ps',array('Y','N'));
            $table->string('picture');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturers');
    }
}
