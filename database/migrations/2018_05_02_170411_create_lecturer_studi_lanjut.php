<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerStudiLanjut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_studi_lanjut', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lecturer_id')->unsigned()->index();
            $table->enum('jenjang',array('S1','S2','S3'));
            $table->integer('pt_id')->unsigned()->index();
            $table->integer('bidang_ahli_id')->unsigned()->index();
            $table->enum('sesuai_bidang',array('Y','N'));
            $table->date('tanggal_tugas');
            $table->date('tanggal_lulus');
            $table->string('no_sk');
            $table->string('sk');
            $table->string('sertifikat');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('lecturer_id')->references('id')->on('lecturers');
            $table->foreign('pt_id')->references('id')->on('master_perguruan_tinggi');
            $table->foreign('bidang_ahli_id')->references('id')->on('master_bidang_keahlian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_studi_lanjut');
    }
}
