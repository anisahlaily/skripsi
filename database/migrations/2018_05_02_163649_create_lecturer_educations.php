<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerEducations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lecturer_id')->unsigned()->index();
            $table->enum('jenjang',array('S1','S2','S3'));
            $table->string('gelar');
            $table->integer('pt_id')->unsigned()->index();
            $table->integer('bidang_ahli_id')->unsigned()->index();
            $table->date('tanggal_lulus');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('lecturer_id')->references('id')->on('lecturers');
            $table->foreign('pt_id')->references('id')->on('master_perguruan_tinggi');
            $table->foreign('bidang_ahli_id')->references('id')->on('master_bidang_keahlian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_educations');
    }
}
