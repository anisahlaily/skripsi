<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturerBeban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_beban', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lecturer_id')->unsigned()->index();
            $table->integer('major_id')->unsigned()->index();
            $table->integer('academic_year_id')->unsigned()->index();
            $table->enum('semester',array('GANJIL','GENAP'));
            $table->enum('kode',array('NGAJAR_PSS','NGAJAR_PSL_PTS','NGAJAR_PTL'));
            $table->float('sks');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('lecturer_id')->references('id')->on('lecturers');
            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('academic_year_id')->references('id')->on('academic_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecturer_beban');
    }
}
