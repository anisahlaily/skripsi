<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMajorStudentServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_major_student_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_id')->unsigned()->index();
            $table->integer('period_id')->unsigned()->index();
            $table->string('type');
            $table->string('quality');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('period_id')->references('id')->on('scoring_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_major_student_services');
    }
}
