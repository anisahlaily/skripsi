<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('faculty_id')->unsigned()->index();
            $table->string('name');
            $table->string('telp');
            $table->string('email');
            $table->string('address');
            $table->string('facsimile');
            $table->string('homepage');
            $table->string('faculty_name');
            $table->string('university');
            $table->integer('city')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('faculty_id')->references('id')->on('faculties');
            $table->foreign('city')->references('id')->on('kota_kabupaten');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
